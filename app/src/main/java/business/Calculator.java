package business;

import exceptions.DivideByZeroException;

public interface Calculator {

    /**
     * Adds two numbers
     * @param num1
     * @param num2
     * @return
     */
    public double add(double num1,double num2);

    /**
     * Subtracts the second number from the first one
     * @param num1
     * @param num2
     * @return
     */
    public double subtract(double num1,double num2);

    /**
     * Divides two numbers
     * Note: Make sure second number isn't 0
     * @param num1
     * @param num2
     * @return
     * @throws DivideByZeroException When trying to divide by zero
     */
    public double divide(double num1, double num2) throws DivideByZeroException;

    /**
     * Multiplies two numbers
     * @param num1
     * @param num2
     * @return
     */
    public double multiply(double num1, double num2);

}
