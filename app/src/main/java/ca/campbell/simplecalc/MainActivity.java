package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import business.Calculator;
import business.SimpleCalculator;
import exceptions.DivideByZeroException;

//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: input validation: set text to show error when it occurs


public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;
    double num1;
    double num2;
    String tag = "CALC";
    private Calculator calc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
        calc = new SimpleCalculator();
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        if (areNumsGiven() && isInputDigits()) {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(calc.add(num1,num2)));
            Log.i(tag, "Adding numbers");
        }
    }

    public void subtractNums(View v) {
        if (areNumsGiven() && isInputDigits()) {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(calc.subtract(num1,num2)));
            Log.i(tag, "Subtracting numbers");
        }
    }

    public void multiplyNums(View v) {
        if (areNumsGiven() && isInputDigits()) {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(calc.multiply(num1,num2)));
            Log.i(tag, "Multiplying numbers");

        }
    }

    public void divideNums(View v) {
        if (areNumsGiven() && isInputDigits()) {
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());

            try {
                result.setText(Double.toString(calc.divide(num1, num2)));
                Log.i(tag,"Dividing numbers");
            } catch (DivideByZeroException e) {
                Toast.makeText(this,"Cannot divide by zero",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void clearFields(View v) {
        etNumber1.setText("");
        etNumber2.setText("");
        result.setText(R.string.result);
    }

    private boolean areNumsGiven() {
        String num1Txt = etNumber1.getText().toString();
        String num2Txt = etNumber2.getText().toString();

        if (num1Txt.equals("") || num2Txt.equals("")) {
            //result.setText("One of the inputs are empty. Enter a number");
            Toast.makeText(this,"One of the inputs are empty. Enter a number",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean isInputDigits() {
        String num1Txt = etNumber1.getText().toString();
        String num2Txt = etNumber2.getText().toString();

        if(!num1Txt.matches("[0-9]+\\.*[0-9]*") || !num2Txt.matches("[0-9]+\\.*[0-9]*")) {
            Toast.makeText(this,"The input must be only digits",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}